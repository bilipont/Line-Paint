import tkinter as tk
from turtle import RawTurtle, TurtleScreen


def forward():
    turtle.forward(100)


def backward():
    turtle.backward(100)


def left():
    turtle.left(90)


def right():
    turtle.right(90)


def clear():
    turtle.clear()

def function1():
    turtle.right(45)

def exit_app():
    window.destroy()


# 创建主窗口
window = tk.Tk()
window.title("Line Paint")

# 创建绘图区域
canvas = tk.Canvas(window, width=600, height=400)
canvas.pack()

screen = TurtleScreen(canvas)
turtle = RawTurtle(screen)

# 创建按钮
forward_btn = tk.Button(window, text="前进", command=forward)
forward_btn.pack(side=tk.LEFT)

backward_btn = tk.Button(window, text="后退", command=backward)
backward_btn.pack(side=tk.LEFT)

left_btn = tk.Button(window, text="左转", command=left)
left_btn.pack(side=tk.LEFT)

right_btn = tk.Button(window, text="右转", command=right)
right_btn.pack(side=tk.LEFT)

qx_btn = tk.Button(window, text="向右倾斜45度", command=function1)
qx_btn.pack(side=tk.LEFT)

clear_btn = tk.Button(window, text="清空", command=clear)
clear_btn.pack(side=tk.LEFT)

exit_btn = tk.Button(window, text="退出", command=exit_app)
exit_btn.pack(side=tk.LEFT)

# 运行主程序
window.mainloop()
